package com.example.suryainterviewtestsaljith.Config;

import com.example.suryainterviewtestsaljith.EmailDetails.DataModel.EmailInputDataModel;
import com.example.suryainterviewtestsaljith.EmailDetails.DataModel.EmailOutputDataModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {

    String BASE_URL = "http://surya-interview.appspot.com";


    @POST("/list")
    Call<EmailOutputDataModel> fetchdetails(@Body EmailInputDataModel body);




}

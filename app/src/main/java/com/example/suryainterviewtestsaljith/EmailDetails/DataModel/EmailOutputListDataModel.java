package com.example.suryainterviewtestsaljith.EmailDetails.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailOutputListDataModel {
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("firstName")
    @Expose
    private String firstName;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


}

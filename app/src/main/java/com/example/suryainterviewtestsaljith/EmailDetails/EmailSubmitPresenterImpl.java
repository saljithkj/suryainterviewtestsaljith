package com.example.suryainterviewtestsaljith.EmailDetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.ArrayList;

public class EmailSubmitPresenterImpl implements EmailSubmitPresenter,EmailSubmitCallBack {

    EmailSubmitView emailSubmitView;
    EmailSubmitModel emailSubmitModel;
    Context mc;


    public EmailSubmitPresenterImpl(EmailSubmitView emailSubmitView,Context context)
    {
        this.emailSubmitView=emailSubmitView;
        emailSubmitModel= new EmailSubmitModelImpl();
        this.mc=context;
    }


    @Override
    public void submitemail(String email) {
        emailSubmitModel.submitemail(email,this);
    }

    @Override
    public void emaildetails(ArrayList email, ArrayList firstname, ArrayList lastname, ArrayList imageurl) {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mc);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(email);

        editor.putString("hh", json);
        editor.commit();
        emailSubmitView.emailsusscess(email,firstname,lastname,imageurl);

    }
}

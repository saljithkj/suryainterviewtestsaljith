package com.example.suryainterviewtestsaljith.EmailDetails;

import android.content.Context;

import com.example.suryainterviewtestsaljith.Config.Api;
import com.example.suryainterviewtestsaljith.EmailDetails.DataModel.EmailInputDataModel;
import com.example.suryainterviewtestsaljith.EmailDetails.DataModel.EmailOutputDataModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmailSubmitModelImpl implements EmailSubmitModel {
    private Context mContext;
    @Override
    public void submitemail(String email, final EmailSubmitCallBack emailSubmitCallBack) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api api = retrofit.create(Api.class);


        Call<EmailOutputDataModel> call = api.fetchdetails(new EmailInputDataModel("saljithkj@gmail.com"));
        call.enqueue(new Callback<EmailOutputDataModel>() {
            @Override
            public void onResponse(Call<EmailOutputDataModel> call, Response<EmailOutputDataModel> response) {


                int size = response.body().getItems().size();

                ArrayList<String> email = new ArrayList<>();
                ArrayList<String> firstname = new ArrayList<>();
                ArrayList<String> lastname = new ArrayList<>();
                ArrayList<String> imageurl = new ArrayList<>();

                for (int i = 0; i < size; i++) {
                    email.add(response.body().getItems().get(i).getEmailId());
                    firstname.add(response.body().getItems().get(i).getFirstName());
                    lastname.add(response.body().getItems().get(i).getLastName());
                    imageurl.add(response.body().getItems().get(i).getImageUrl());
                }







                emailSubmitCallBack.emaildetails(email, firstname, lastname, imageurl);

            }

            @Override
            public void onFailure(Call<EmailOutputDataModel> call, Throwable t) {

            }
        });


    }


}

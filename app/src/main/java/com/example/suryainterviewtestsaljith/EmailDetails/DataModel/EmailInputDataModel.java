package com.example.suryainterviewtestsaljith.EmailDetails.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailInputDataModel {
    @SerializedName("emailId")
    @Expose
    private String emailId;

    public EmailInputDataModel(String s) {
        this.emailId=s;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

}
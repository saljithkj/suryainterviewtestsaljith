package com.example.suryainterviewtestsaljith.EmailDetails.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmailOutputDataModel {@SerializedName("items")
@Expose
private List<EmailOutputListDataModel> items = null;

    public List<EmailOutputListDataModel> getItems() {
        return items;
    }

    public void setItems(List<EmailOutputListDataModel> items) {
        this.items = items;
    }

}
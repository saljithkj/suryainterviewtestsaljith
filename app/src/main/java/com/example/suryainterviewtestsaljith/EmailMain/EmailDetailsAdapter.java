package com.example.suryainterviewtestsaljith.EmailMain;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.suryainterviewtestsaljith.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

class EmailDetailsAdapter extends RecyclerView.Adapter<EmailDetailsAdapter.Viewholder> {

    ArrayList<String> email = new ArrayList<>();
    ArrayList<String> firstname = new ArrayList<>();
    ArrayList<String> lastname = new ArrayList<>();
    ArrayList<String> imageurl = new ArrayList<>();
    Context context;

    public EmailDetailsAdapter(ArrayList email, ArrayList firstname, ArrayList lastname, ArrayList imageurl, Context context) {
   this.email=email;
   this.firstname=firstname;
   this.lastname=lastname;
   this.imageurl=imageurl;
   this.context=context;


    }

    @NonNull
    @Override
    public EmailDetailsAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardviewemaildetails,viewGroup,false);
        return new Viewholder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull EmailDetailsAdapter.Viewholder viewholder, int i) {

viewholder.email.setText(email.get(i));
viewholder.firstname.setText(firstname.get(i));
viewholder.lastname.setText(lastname.get(i));

String image=imageurl.get(i);

            Glide.with(context).load(imageurl.get(i)).into(viewholder.circleImageView);
//
//


    }


    @Override
    public int getItemCount() {
        return email.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView email,firstname,lastname;
        CircleImageView circleImageView;
        public Viewholder(@NonNull View itemView) {
            super(itemView);

            email=itemView.findViewById(R.id.tv_email);
            firstname=itemView.findViewById(R.id.tv_firstname);
            lastname=itemView.findViewById(R.id.tv_lastname);
            circleImageView=itemView.findViewById(R.id.img_profile_pic);
        }
    }
}

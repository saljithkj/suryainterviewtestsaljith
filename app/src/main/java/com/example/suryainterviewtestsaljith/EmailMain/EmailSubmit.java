package com.example.suryainterviewtestsaljith.EmailMain;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.suryainterviewtestsaljith.EmailDetails.EmailSubmitPresenter;
import com.example.suryainterviewtestsaljith.EmailDetails.EmailSubmitPresenterImpl;
import com.example.suryainterviewtestsaljith.EmailDetails.EmailSubmitView;
import com.example.suryainterviewtestsaljith.R;


import java.util.ArrayList;

public class EmailSubmit extends AppCompatActivity implements EmailSubmitView {
EmailSubmitPresenter emailSubmitPresenter;

    RecyclerView recyclerView;
    LinearLayoutManager mLayoutManager;
EditText email1;
Button submitt1;
ProgressBar pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_submit);

email1=findViewById(R.id.et_email);
submitt1=findViewById(R.id.submitt);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_main);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        emailSubmitPresenter=new EmailSubmitPresenterImpl(this,getApplicationContext());
        submitt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String em=email1.getText().toString();

                emailSubmitPresenter.submitemail(em);

            }
        });


    }

    @Override
    public void emailsusscess(ArrayList email, ArrayList firstname, ArrayList lastname, ArrayList imageurl) {
        Log.e("hello",email.toString());
        email1.setVisibility(View.INVISIBLE);
        submitt1.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        EmailDetailsAdapter adapter=new EmailDetailsAdapter(email,firstname,lastname,imageurl,getApplicationContext());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {

        email1.setVisibility(View.VISIBLE);
        submitt1.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);

    }

    @Override
    public void emailfaild() {

    }
}
